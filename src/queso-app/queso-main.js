import { LitElement, html, css } from 'lit-element';
import '../ficha-queso/ficha-queso-plantilla.js';
import '../ficha-queso/ficha-queso-info.js';
class QuesoMain extends LitElement {

	static get properties() {
		return {
			cheeseData: { type: Array }
		};
	}

	constructor() {
		// Always calls super() first.
		super();
		this.cheeseData = [];
		this.getCheeseData();
	}

	static get styles() {
		return css`
			div
			{
				background-color: lightblue;	
			}
			.row{
				margin:auto
			}
		`;
	}


	render() {
		return html`
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
			<main>
				<div class="row" id="cheeseList">
					<div class="row row-cols-1 row-cols-md-4 mb-3">
					${this.cheeseData.map(cheese => {
								return html`
									<ficha-queso-plantilla 
										name="${cheese.name}" 
										photo="${cheese.image.src}" 
										age="${cheese.age}"
										.recommended="${cheese.recommended}"
										@info-cheese=${this.infoCheese}
										@delete-cheese=${this.deleteCheese}												
									>
									</ficha-queso-plantilla>
								`;
							})}
				   </div>
				</div>   
				   <div class="row">
					<ficha-queso-info id="quesoFicha" class="border rounded border-secondary d-none"
					@info-form-close="${this.closeCheeseForm}"
					@queso-form-store="${this.storeCheeseForm}"
					>
					</ficha-queso-info>
				</div>
				</div>				
			</main>
		`;
	}

	getCheeseData() {
		console.log("getCheeseData");
		console.log("Obteniendo datos de quesos");

		let xhr = new XMLHttpRequest();

		xhr.onload = function () {

			if (xhr.status === 200) {
				console.log("Peticion completada correctamente");

				let APIResponse = JSON.parse(xhr.responseText);

				this.cheeseData = APIResponse.cheeses;
				console.log(this.cheeseData);
			}

		}.bind(this);

		xhr.open("GET", "http://localhost:8001/data/quesos.json");
		xhr.send();
	}

	deleteCheese(e) {
		console.log("Se va a borrar el queso con el nombre" + e.detail.name);
	
		this.cheeseData = this.cheeseData.filter(cheese => cheese.name != e.detail.name);
	}

	infoCheese(e) {
		let selecteCheese = this.cheeseData.filter(cheese => cheese.name == e.detail.name)
		this.shadowRoot.getElementById("quesoFicha").cheese = selecteCheese[0];
	    this.showCheeseForm();
	 }

	showCheeseForm( newCheese){
		 this.shadowRoot.getElementById("quesoFicha").classList.remove("d-none")
		 this.shadowRoot.getElementById("cheeseList").classList.add("d-none")
		 
		 if(newCheese){
			 this.shadowRoot.getElementById("quesoFicha").newCheese = true;
		 }
	 }

	 closeCheeseForm() {
		console.log("quesoFormClose");
		console.log("Se ha cerrado el formulario de quesos");
		this.shadowRoot.getElementById("cheeseList").classList.remove("d-none");	  
		this.shadowRoot.getElementById("quesoFicha").classList.add("d-none");	
    }

	storeCheeseForm(e) {
		console.log("actualizar o gurdar nuevo");
	
		let findCheese = this.cheeseData.findIndex(cheese => cheese.name === e.detail.cheese.name)
		if(findCheese>=0){
			this.cheeseData[findCheese].age = e.detail.cheese.age;
			this.cheeseData[findCheese].description = e.detail.cheese.description;
			this.requestUpdate();
		}else{
			this.cheese.push(e.detail.cheese);
		}  
		console.log("queso almacenada");	
		this.closeCheeseForm();
	}

}
customElements.define('queso-main', QuesoMain);
