import { LitElement, html, css } from 'lit-element';

class QuesoHeader extends LitElement {

	static get properties() {
		return {

		};
	}


	static get styles() {
		return css`
      			div{
				background:#888;
				padding: 20px 10px;
				text-align:center;
				overflow: hidden;
				color:#ccc;
			}`
	}


	constructor() {
		// Always calls super() first.
		super();

	}

	render() {
		return html`
			<div>QUESOS</div>
		`;
	}

}

customElements.define('queso-header', QuesoHeader);
