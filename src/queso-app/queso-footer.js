import { LitElement, html, css } from 'lit-element';  

class QuesoFooter extends LitElement {   

	static get properties() {
		return {
		};
	}

	static get styles()
	{
		return css `
			.footer 
			{
				position:fixed;
				bottom:0px;
				background:#888;
				color:#ccc;
				width:100%;
				padding:10px,10px;
				overflow: hidden;
				height:6%;
				text-align:center;
			}
		`;
	
	}

	constructor() {
	// Always calls super() first.
	super();

	}	


	render() {
		return html`
			<div class='footer'>@QUESOS.COM </div>
		`;
	}

}  

customElements.define('queso-footer', QuesoFooter); 
