import {LitElement, html,css} from 'lit-element';

/**
 * `LowerCaseDashedName` Description
 *
 * @customElement
 * @polymer
 * @demo
 * 
 */
class QuesoLeft extends LitElement {
   
    constructor() {
        // Always calls super() first.
        super();
    
        }	

    static get properties() {
        return {

        }
    }
    static get styles()
	{
		return css `
			.left 
			{   
                float: left;
                height:100%;
                width:15%;
                color:#ccc;
                padding:20px
                overflow: auto;
    				
			}
		`;
	
	}

    /**
     * Implement to describe the element's DOM using lit-html.
     * Use the element current props to return a lit-html template result
     * to render into the element.
     */
    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">		
            <div class="left">
                <button type="button" class="btn btn-success btn-lg mt-3 ml-2 mb-1 mr-1">Agregar</button>
            </div>
        `;
    }

}

customElements.define('queso-left', QuesoLeft);