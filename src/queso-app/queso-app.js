import {LitElement, html} from 'lit-element';
import './queso-main.js';
import './queso-footer.js';
import './queso-header.js';
import './queso-left.js';
//import '../ficha-queso/ficha-queso-info.js';
/**
 * `LowerCaseDashedName` Description
 *
 * @customElement
 * @polymer
 * @demo
 * 
 */
class QuesoApp extends LitElement {
    static get properties() {
        return {

        }
    }

    /**
     * Instance of the element is created/upgraded. Use: initializing state,
     * set up event listeners, create shadow dom.
     * @constructor
     */
    constructor() {
        super();
    }



    /**
     * Implement to describe the element's DOM using lit-html.
     * Use the element current props to return a lit-html template result
     * to render into the element.
     */
    render() {
        return html`
            <queso-header></queso-header>
            <queso-left></queso-left>
            <queso-main></queso-main>
            <queso-footer></queso-footer>
        `;
    }

}

customElements.define('queso-app', QuesoApp);