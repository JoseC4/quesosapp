import { LitElement, html, css } from 'lit-element';

/**
 * `LowerCaseDashedName` Description
 *
 * @customElement
 * @polymer
 * @demo
 * 
 */
class FichaQuesoInfo extends LitElement {
    static get properties() {
        return {
            cheese: { type: Object }
        }
    }

    /**
     * Instance of the element is created/upgraded. Use: initializing state,
     * set up event listeners, create shadow dom.
     * @constructor
     */
    constructor() {
        super();
        this.cheese = this.cheese || {};
        this.cheese.name = this.cheese.name || "";
        this.cheese.description = this.cheese.description || "";
        this.cheese.photo = this.cheese.photo || "";
    }

    static get styles() {

    }

    /**
     * Implement to describe the element's DOM using lit-html.
     * Use the element current props to return a lit-html template result
     * to render into the element.
     */
    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        
        <div class="border rounded border-secondary">
            <div class="modal-header bg-info text-white">
                <h3>Queso Info</h3>
            </div>
        
            <div class="border m-2 p-2">
                <form>
                    <div class="form-group">
                        <label> Nombre</label>
                        <input type="text" id="formName" class="form-control " .value=${this.cheese.name} readonly>
                    </div>
        
                    <div class="form-group">
                        <label> Descripci&oacute;n</label>
                        <textarea class="form-control" @input="${this.updateCheeseDecription}" placeholder="Descripcion" rows="4">
                        ${this.cheese.description}</textarea>
                    </div>
        
                    <div class="form-group">
                        <label> Meses de maduraci&aacute;n</label>
                        <input type="text" @input="${this.updateCheeseAge}" class="form-control" placeholder="Meses de maduracion"
                            .value=${this.cheese.age}>
                    </div>
        
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" @click="${this.storeCheese}" class="btn btn-primary">Save changes</button>
                <button type="button" @click="${this.goBack}" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        
        </div>
       
        
        `;
    }

    goBack(e) {
        console.log("goBack");
        e.preventDefault();
        this.dispatchEvent(new CustomEvent("info-form-close", {}));
    }


    storeCheese(e) {
        console.log("storeQueso");
        e.preventDefault();

        console.log("La propiedad description" + this.cheese.description);
        console.log("La propiedad age " + this.cheese.age);

        this.dispatchEvent(new CustomEvent("queso-form-store", {
            detail: {
                cheese: {
                    name:this.cheese.name,
                    description: this.cheese.description,
                    age: this.cheese.age,
                }
            }
        })
        );
    }

    updateCheeseDecription(e) {
        console.log("Actualizando la propiedad description" + e.target.value);
        this.cheese.description = e.target.value;
        //this.requestUpdate();
    }

    updateCheeseAge(e) {
        console.log("Actualizando la propiedad age" + e.target.value);
        this.cheese.age = e.target.value;
        //this.requestUpdate();
    }

}

customElements.define('ficha-queso-info', FichaQuesoInfo);