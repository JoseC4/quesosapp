import { LitElement, html } from 'lit-element';
import '../ficha-queso/ficha-queso-info.js';

export class FichaQuesoPlantilla extends LitElement {

    /**
      * Declared properties and their corresponding attributes
      */
    static get properties() {
        return {
            name:   {type: String},			
		    photo:  {type: String},			
            age:    {type: Number},
            recommended: {type: Boolean },
        };
    }

    constructor() {
        // Always calls super() first.
        super();
        //this.name="sin agregar";
        //this.photo="";	
        //this.age=0;
        //this.recommended=false;
        //this.description="";
    }	


    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">		
        <div class="card border-info mb-4 mt-4  ml-2 mr-4" style="width:350px">
            <div class="card-header text-center">${this.name}</div>
            <div class="card-img-top"><img src="${this.photo}" height="300" width="349"></div>
            <div class="card-text mt-1">
                  <p class="text-center font-weight-bold">Edad :${this.age}</p>
            </div>
            <div class="text-center mt-1 mb-1">
                ${this.recommended? 
                 html`<img src="img/star.png" height="30" width="30">` :
                 html`<img src="img/emptyStar.png" height="30" width="30">`}
            </div>

            <div class="card-footer">
                <a href="#" class="btn btn-info"   @click=${this.infoCheese}>Info</a>
                <a href="#" class="btn btn-danger" @click=${this.deletePerson}>Borrar</a>
            </div>
           
        </div>   
        `;
    }

    infoCheese()
	{
		this.dispatchEvent(new CustomEvent('info-cheese',{detail:{'name':this.name}}));
	}

    deletePerson(e) {	
		console.log("Se va borrar el queso" + this.name); 

		this.dispatchEvent(
			new CustomEvent("delete-cheese", {
					detail: {
						name: this.name
					}
				}
			)
		);
	}
}
customElements.define('ficha-queso-plantilla', FichaQuesoPlantilla);